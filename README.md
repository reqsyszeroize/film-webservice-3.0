# API de Réservation de Séances de Cinéma

## Contexte
Notre site de consultation de films ayant du succès, nous souhaitons désormais permettre aux utilisateurs de réserver leurs séances via notre plateforme.

## Configuration

### Installation des dépendances

```bash
pip install -r requirements.txt

Exécution de l'application

bash

python run.py

Routes de l'API
Films

    GET /movies : Liste des films enregistrés.
    GET /movies/{uid} : Récupérer un film.
    POST /movies : Créer un film.
    PUT /movies/{uid} : Modifier un film.
    DELETE /movies/{uid} : Supprimer un film.

Réservations

    POST /movie/{movieUid}/reservations : Créer une réservation.
    POST /reservations/{uid}/confirm : Confirmer une réservation.
    GET /movie/{movieUid}/reservations : Liste des réservations pour un film.
    GET /reservations/{uid} : Détails d'une réservation.

Cinémas

    GET /cinema : Liste des cinémas.
    GET /cinema/{uid} : Récupérer un cinéma.
    POST /cinema : Créer un cinéma.
    PUT /cinema/{uid} : Modifier un cinéma.
    DELETE /cinema/{uid} : Supprimer un cinéma.

Salles

    GET /cinema/{cinemaUid}/rooms : Liste des salles de cinéma.
    GET /cinema/{cinemaUid}/rooms/{uid} : Récupérer une salle de cinéma.
    POST /cinema/{cinemaUid}/rooms : Créer une salle de cinéma.
    PUT /cinema/{cinemaUid}/rooms/{uid} : Modifier une salle de cinéma.
    DELETE /cinema/{cinemaUid}/rooms/{uid} : Supprimer une salle de cinéma.

Séances

    GET /cinema/{cinemaUid}/rooms/{roomUid}/sceances : Liste des séances d'une salle.
    POST /cinema/{cinemaUid}/rooms/{roomUid}/sceances : Créer une séance.
    PUT /cinema/{cinemaUid}/rooms/{roomUid}/sceances/{uid} : Modifier une séance.
    DELETE /cinema/{cinemaUid}/rooms/{roomUid}/sceances/{uid} : Supprimer une séance.

Modèles de Données
Film

    uid : UUID V4
    name : String (128)
    description : String (4096)
    rate : Integer (1-5)
    duration : Integer (1-240)
    hasReservationsAvailable : Boolean
    createdAt : DateTime
    updatedAt : DateTime

Réservation

    uid : UUID V4
    movieUid : UUID V4
    sceance : DateTime
    nbSeats : Integer
    room : String (36)
    rank : Integer
    status : String (open, expired, confirmed)
    createdAt : DateTime
    updatedAt : DateTime
    expiresAt : DateTime

Cinéma

    uid : UUID V4
    name : String (128)
    createdAt : DateTime
    updatedAt : DateTime

Salle

    uid : UUID V4
    cinemaUid : UUID V4
    seats : Integer
    name : String (128)
    createdAt : DateTime
    updatedAt : DateTime

Séance

    uid : UUID V4
    movieUid : UUID V4
    roomUid : UUID V4
    date : DateTime
    createdAt : DateTime
    updatedAt : DateTime