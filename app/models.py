from datetime import datetime

from . import db 

class Movie(db.Model):
    uid = db.Column(db.String(36), primary_key=True)
    name = db.Column(db.String(128), nullable=False)
    description = db.Column(db.String(4096), nullable=False)
    rate = db.Column(db.Integer, nullable=False)
    duration = db.Column(db.Integer, nullable=False)
    hasReservationsAvailable = db.Column(db.Boolean, default=True)
    createdAt = db.Column(db.DateTime, default=datetime.utcnow)
    updatedAt = db.Column(db.DateTime, default=datetime.utcnow, onupdate=datetime.utcnow)

class Reservation(db.Model):
    uid = db.Column(db.String(36), primary_key=True)
    movieUid = db.Column(db.String(36), db.ForeignKey('movie.uid'), nullable=False)
    sceance = db.Column(db.DateTime, nullable=False)
    nbSeats = db.Column(db.Integer, nullable=False)
    room = db.Column(db.String(36), nullable=False)
    rank = db.Column(db.Integer, nullable=False)
    status = db.Column(db.String(50), nullable=False)
    createdAt = db.Column(db.DateTime, default=datetime.utcnow)
    updatedAt = db.Column(db.DateTime, default=datetime.utcnow, onupdate=datetime.utcnow)
    expiresAt = db.Column(db.DateTime, nullable=False)

class Cinema(db.Model):
    uid = db.Column(db.String(36), primary_key=True)
    name = db.Column(db.String(128), nullable=False)
    createdAt = db.Column(db.DateTime, default=datetime.utcnow)
    updatedAt = db.Column(db.DateTime, default=datetime.utcnow, onupdate=datetime.utcnow)

class Room(db.Model):
    uid = db.Column(db.String(36), primary_key=True)
    cinemaUid = db.Column(db.String(36), db.ForeignKey('cinema.uid'), nullable=False)
    seats = db.Column(db.Integer, nullable=False)
    name = db.Column(db.String(128), nullable=False)
    createdAt = db.Column(db.DateTime, default=datetime.utcnow)
    updatedAt = db.Column(db.DateTime, default=datetime.utcnow, onupdate=datetime.utcnow)

class Sceance(db.Model):
    uid = db.Column(db.String(36), primary_key=True)
    movieUid = db.Column(db.String(36), db.ForeignKey('movie.uid'), nullable=False)
    roomUid = db.Column(db.String(36), db.ForeignKey('room.uid'), nullable=False)
    date = db.Column(db.DateTime, nullable=False)
    createdAt = db.Column(db.DateTime, default=datetime.utcnow)
    updatedAt = db.Column(db.DateTime, default=datetime.utcnow, onupdate=datetime.utcnow)