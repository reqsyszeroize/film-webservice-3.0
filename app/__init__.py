from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate

db = SQLAlchemy()
migrate = Migrate()

def create_app():
    app = Flask(__name__)
    app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///movies.db'
    app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
    
    db.init_app(app)
    
    from .routes.movies import movies_bp
    from .routes.reservations import reservations_bp
    from .routes.cinemas import cinemas_bp
    from .routes.rooms import rooms_bp
    from .routes.sceances import sceances_bp

    app.register_blueprint(movies_bp, url_prefix='/movies')
    app.register_blueprint(reservations_bp, url_prefix='/reservations')
    app.register_blueprint(cinemas_bp, url_prefix='/cinemas')
    app.register_blueprint(rooms_bp, url_prefix='/rooms')
    app.register_blueprint(sceances_bp, url_prefix='/sceances')

    migrate.init_app(app, db)

    return app