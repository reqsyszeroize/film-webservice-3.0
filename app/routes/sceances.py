from flask import Blueprint, request, jsonify
from app.models import db, Sceance
import uuid
from datetime import datetime

sceances_bp = Blueprint('sceances', __name__)

@sceances_bp.route('/<string:cinemaUid>/rooms/<string:roomUid>/sceances', methods=['GET'])
def get_sceances(cinemaUid, roomUid):
    sceances = Sceance.query.filter_by(roomUid=roomUid).all()
    if not sceances:
        return '', 204
    return jsonify([sceance.to_dict() for sceance in sceances]), 200

@sceances_bp.route('/<string:cinemaUid>/rooms/<string:roomUid>/sceances/<string:uid>', methods=['GET'])
def get_sceance(cinemaUid, roomUid, uid):
    sceance = Sceance.query.filter_by(roomUid=roomUid, uid=uid).first()
    if not sceance:
        return '', 404
    return jsonify(sceance.to_dict()), 200

@sceances_bp.route('/<string:cinemaUid>/rooms/<string:roomUid>/sceances', methods=['POST'])
def create_sceance(cinemaUid, roomUid):
    data = request.get_json()
    try:
        sceance = Sceance(
            uid=str(uuid.uuid4()),
            movieUid=data['movieUid'],
            roomUid=roomUid,
            date=data['date'],
            createdAt=datetime.utcnow(),
            updatedAt=datetime.utcnow()
        )
        db.session.add(sceance)
        db.session.commit()
        return jsonify(sceance.to_dict()), 201
    except Exception as e:
        return jsonify({"error": str(e)}), 422

@sceances_bp.route('/<string:cinemaUid>/rooms/<string:roomUid>/sceances/<string:uid>', methods=['PUT'])
def update_sceance(cinemaUid, roomUid, uid):
    data = request.get_json()
    sceance = Sceance.query.filter_by(roomUid=roomUid, uid=uid).first()
    if not sceance:
        return '', 404
    try:
        sceance.movieUid = data['movieUid']
        sceance.date = data['date']
        sceance.updatedAt = datetime.utcnow()
        db.session.commit()
        return jsonify(sceance.to_dict()), 200
    except Exception as e:
        return jsonify({"error": str(e)}), 422

@sceances_bp.route('/<string:cinemaUid>/rooms/<string:roomUid>/sceances/<string:uid>', methods=['DELETE'])
def delete_sceance(cinemaUid, roomUid, uid):
    sceance = Sceance.query.filter_by(roomUid=roomUid, uid=uid).first()
    if not sceance:
        return '', 404
    db.session.delete(sceance)
    db.session.commit()
    return '', 204

# Helper function to serialize model to dict
def sceance_to_dict(sceance):
    return {
        'uid': sceance.uid,
        'movieUid': sceance.movieUid,
        'roomUid': sceance.roomUid,
        'date': sceance.date,
        'createdAt': sceance.createdAt,
        'updatedAt': sceance.updatedAt
    }

Sceance.to_dict = sceance_to_dict