from flask import Blueprint, request, jsonify
from app.models import db, Cinema
import uuid
from datetime import datetime

cinemas_bp = Blueprint('cinemas', __name__)

@cinemas_bp.route('/', methods=['GET'])
def get_cinemas():
    cinemas = Cinema.query.all()
    if not cinemas:
        return '', 204
    return jsonify([cinema.to_dict() for cinema in cinemas]), 200

@cinemas_bp.route('/<string:uid>', methods=['GET'])
def get_cinema(uid):
    cinema = Cinema.query.get(uid)
    if not cinema:
        return '', 404
    return jsonify(cinema.to_dict()), 200

@cinemas_bp.route('/', methods=['POST'])
def create_cinema():
    data = request.get_json()
    try:
        cinema = Cinema(
            uid=str(uuid.uuid4()),
            name=data['name'],
            createdAt=datetime.utcnow(),
            updatedAt=datetime.utcnow()
        )
        db.session.add(cinema)
        db.session.commit()
        return jsonify(cinema.to_dict()), 201
    except Exception as e:
        return jsonify({"error": str(e)}), 422

@cinemas_bp.route('/<string:uid>', methods=['PUT'])
def update_cinema(uid):
    data = request.get_json()
    cinema = Cinema.query.get(uid)
    if not cinema:
        return '', 404
    try:
        cinema.name = data['name']
        cinema.updatedAt = datetime.utcnow()
        db.session.commit()
        return jsonify(cinema.to_dict()), 200
    except Exception as e:
        return jsonify({"error": str(e)}), 422

@cinemas_bp.route('/<string:uid>', methods=['DELETE'])
def delete_cinema(uid):
    cinema = Cinema.query.get(uid)
    if not cinema:
        return '', 404
    db.session.delete(cinema)
    db.session.commit()
    return '', 204

# Helper function to serialize model to dict
def cinema_to_dict(cinema):
    return {
        'uid': cinema.uid,
        'name': cinema.name,
        'createdAt': cinema.createdAt,
        'updatedAt': cinema.updatedAt
    }

Cinema.to_dict = cinema_to_dict