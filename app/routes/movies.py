from flask import Blueprint, request, jsonify
from app.models import db, Movie
import uuid
from datetime import datetime

movies_bp = Blueprint('movies', __name__)

@movies_bp.route('/', methods=['GET'])
def get_movies():
    movies = Movie.query.all()
    if not movies:
        return '', 204
    return jsonify([movie.to_dict() for movie in movies]), 200

@movies_bp.route('/<string:uid>', methods=['GET'])
def get_movie(uid):
    movie = Movie.query.get(uid)
    if not movie:
        return '', 404
    return jsonify(movie.to_dict()), 200

@movies_bp.route('/', methods=['POST'])
def create_movie():
    data = request.get_json()
    try:
        movie = Movie(
            uid=str(uuid.uuid4()),
            name=data['name'],
            description=data['description'],
            rate=data['rate'],
            duration=data['duration'],
            hasReservationsAvailable=False,
            createdAt=datetime.utcnow(),
            updatedAt=datetime.utcnow()
        )
        db.session.add(movie)
        db.session.commit()
        return jsonify(movie.to_dict()), 201
    except Exception as e:
        return jsonify({"error": str(e)}), 422

@movies_bp.route('/<string:uid>', methods=['PUT'])
def update_movie(uid):
    data = request.get_json()
    movie = Movie.query.get(uid)
    if not movie:
        return '', 404
    try:
        movie.name = data['name']
        movie.description = data['description']
        movie.rate = data['rate']
        movie.duration = data['duration']
        movie.updatedAt = datetime.utcnow()
        db.session.commit()
        return jsonify(movie.to_dict()), 200
    except Exception as e:
        return jsonify({"error": str(e)}), 422

@movies_bp.route('/<string:uid>', methods=['DELETE'])
def delete_movie(uid):
    movie = Movie.query.get(uid)
    if not movie:
        return '', 404
    db.session.delete(movie)
    db.session.commit()
    return '', 204

# Helper function to serialize model to dict
def movie_to_dict(movie):
    return {
        'uid': movie.uid,
        'name': movie.name,
        'description': movie.description,
        'rate': movie.rate,
        'duration': movie.duration,
        'hasReservationsAvailable': movie.hasReservationsAvailable,
        'createdAt': movie.createdAt,
        'updatedAt': movie.updatedAt
    }

Movie.to_dict = movie_to_dict