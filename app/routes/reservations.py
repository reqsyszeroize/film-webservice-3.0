from flask import Blueprint, request, jsonify
from app.models import db, Reservation, Movie
import uuid
from datetime import datetime

reservations_bp = Blueprint('reservations', __name__)

@reservations_bp.route('/<string:movieUid>/reservations', methods=['POST'])
def create_reservation(movieUid):
    data = request.get_json()
    try:
        reservation = Reservation(
            uid=str(uuid.uuid4()),
            movieUid=movieUid,
            sceance=data['sceance'],
            nbSeats=data['nbSeats'],
            room=data['room'],
            rank=data['rank'],
            status='open',
            createdAt=datetime.utcnow(),
            updatedAt=datetime.utcnow(),
            expiresAt=data['expiresAt']
        )
        db.session.add(reservation)
        db.session.commit()
        return jsonify(reservation.to_dict()), 201
    except Exception as e:
        return jsonify({"error": str(e)}), 422

@reservations_bp.route('/<string:uid>/confirm', methods=['POST'])
def confirm_reservation(uid):
    reservation = Reservation.query.get(uid)
    if not reservation:
        return '', 404
    if reservation.status != 'open':
        return '', 422
    try:
        reservation.status = 'confirmed'
        reservation.updatedAt = datetime.utcnow()
        db.session.commit()
        return jsonify(reservation.to_dict()), 200
    except Exception as e:
        return jsonify({"error": str(e)}), 500

@reservations_bp.route('/movie/<string:movieUid>/reservations', methods=['GET'])
def get_reservations_by_movie(movieUid):
    reservations = Reservation.query.filter_by(movieUid=movieUid).all()
    if not reservations:
        return '', 204
    return jsonify([reservation.to_dict() for reservation in reservations]), 200

@reservations_bp.route('/<string:uid>', methods=['GET'])
def get_reservation(uid):
    reservation = Reservation.query.get(uid)
    if not reservation:
        return '', 404
    return jsonify(reservation.to_dict()), 200

# Helper function to serialize model to dict
def reservation_to_dict(reservation):
    return {
        'uid': reservation.uid,
        'movieUid': reservation.movieUid,
        'sceance': reservation.sceance,
        'nbSeats': reservation.nbSeats,
        'room': reservation.room,
        'rank': reservation.rank,
        'status': reservation.status,
        'createdAt': reservation.createdAt,
        'updatedAt': reservation.updatedAt,
        'expiresAt': reservation.expiresAt
    }

Reservation.to_dict = reservation_to_dict