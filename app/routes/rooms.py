from flask import Blueprint, request, jsonify
from app.models import db, Room
import uuid
from datetime import datetime

rooms_bp = Blueprint('rooms', __name__)

@rooms_bp.route('/<string:cinemaUid>/rooms', methods=['GET'])
def get_rooms(cinemaUid):
    rooms = Room.query.filter_by(cinemaUid=cinemaUid).all()
    if not rooms:
        return '', 204
    return jsonify([room.to_dict() for room in rooms]), 200

@rooms_bp.route('/<string:cinemaUid>/rooms/<string:uid>', methods=['GET'])
def get_room(cinemaUid, uid):
    room = Room.query.filter_by(cinemaUid=cinemaUid, uid=uid).first()
    if not room:
        return '', 404
    return jsonify(room.to_dict()), 200

@rooms_bp.route('/<string:cinemaUid>/rooms', methods=['POST'])
def create_room(cinemaUid):
    data = request.get_json()
    try:
        room = Room(
            uid=str(uuid.uuid4()),
            cinemaUid=cinemaUid,
            seats=data['seats'],
            name=data['name'],
            createdAt=datetime.utcnow(),
            updatedAt=datetime.utcnow()
        )
        db.session.add(room)
        db.session.commit()
        return jsonify(room.to_dict()), 201
    except Exception as e:
        return jsonify({"error": str(e)}), 422

@rooms_bp.route('/<string:cinemaUid>/rooms/<string:uid>', methods=['PUT'])
def update_room(cinemaUid, uid):
    data = request.get_json()
    room = Room.query.filter_by(cinemaUid=cinemaUid, uid=uid).first()
    if not room:
        return '', 404
    try:
        room.seats = data['seats']
        room.name = data['name']
        room.updatedAt = datetime.utcnow()
        db.session.commit()
        return jsonify(room.to_dict()), 200
    except Exception as e:
        return jsonify({"error": str(e)}), 422

@rooms_bp.route('/<string:cinemaUid>/rooms/<string:uid>', methods=['DELETE'])
def delete_room(cinemaUid, uid):
    room = Room.query.filter_by(cinemaUid=cinemaUid, uid=uid).first()
    if not room:
        return '', 404
    db.session.delete(room)
    db.session.commit()
    return '', 204

# Helper function to serialize model to dict
def room_to_dict(room):
    return {
        'uid': room.uid,
        'cinemaUid': room.cinemaUid,
        'seats': room.seats,
        'name': room.name,
        'createdAt': room.createdAt,
        'updatedAt': room.updatedAt
    }

Room.to_dict = room_to_dict